defmodule Aoc2023.D09 do
  def solve_a() do
    File.stream!("inputs/d09a.txt")
    |> Stream.map(&parse_line/1)
    |> Stream.map(&Enum.reverse/1)
    |> Stream.map(&solve_line_a/1)
    |> Enum.sum()
    |> IO.puts()
  end

  def solve_b() do
    File.stream!("inputs/d09a.txt")
    |> Stream.map(&parse_line/1)
    |> Stream.map(&solve_line_a/1)
    |> Enum.sum()
    |> IO.puts()
  end

  def add_diffs(levels, level) do
    diffs =
      Map.fetch!(levels, level)
      |> Enum.chunk_every(2, 1, :discard)
      |> Enum.map(fn [n1, n2] -> n1 - n2 end)

    all_zeros =
      Enum.reduce_while(diffs, true, fn diff, _ ->
        if diff !== 0 do
          {:halt, false}
        else
          {:cont, true}
        end
      end)

    if all_zeros do
      levels
    else
      levels = Map.put(levels, level + 1, diffs)
      add_diffs(levels, level + 1)
    end
  end

  def solve_line_a(nums) do
    levels = %{}
    levels = Map.put(levels, 1, nums)
    levels = add_diffs(levels, 1)

    result =
      Enum.reduce(levels, 0, fn {_level, [diff | _]}, acc ->
        acc + diff
      end)

    result
  end

  def parse_line(line) do
    String.split(line)
    |> Enum.map(&String.to_integer/1)
  end
end
