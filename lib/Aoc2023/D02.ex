defmodule Aoc2023.D02 do
  def solve_a() do
    limit_red = 12
    limit_green = 13
    limit_blue = 14

    solve_path("inputs/d02a.txt", limit_red, limit_green, limit_blue, &solve_line_a/4)
    |> IO.puts()
  end

  def solve_b() do
    limit_red = 12
    limit_green = 13
    limit_blue = 14

    solve_path("inputs/d02b.txt", limit_red, limit_green, limit_blue, &solve_line_b/4)
    |> IO.puts()
  end

  def solve_path(path, limit_red, limit_green, limit_blue, solve_line_fn) do
    File.stream!(path)
    |> solve_stream(limit_red, limit_green, limit_blue, solve_line_fn)
  end

  def solve_stream(lines, limit_red, limit_green, limit_blue, solve_line_fn) do
    lines
    |> Stream.map(fn line -> solve_line_fn.(line, limit_red, limit_green, limit_blue) end)
    |> Enum.sum()
  end

  def solve_line_a(line, limit_red, limit_green, limit_blue) do
    {game_id, takes} = parse_line(line)
    maxs = max_takes(takes)
    max_red = Map.get(maxs, "red", 0)
    max_green = Map.get(maxs, "green", 0)
    max_blue = Map.get(maxs, "blue", 0)

    if(max_red <= limit_red and max_green <= limit_green and max_blue <= limit_blue) do
      game_id
    else
      0
    end
  end

  def solve_line_b(line, _limit_red, _limit_green, _limit_blue) do
    {_game_id, takes} = parse_line(line)
    maxs = max_takes(takes)
    max_red = Map.get(maxs, "red", 0)
    max_green = Map.get(maxs, "green", 0)
    max_blue = Map.get(maxs, "blue", 0)

    max_red * max_green * max_blue
  end

  def parse_line(line) do
    [game_id_str, takes_str] = String.split(line, ":")
    [_, game_id_str] = String.split(game_id_str, " ", trim: true)
    game_id = String.to_integer(game_id_str)

    takes_str = String.trim(takes_str)

    takes =
      if takes_str == "" do
        []
      else
        String.split(takes_str, ";", trim: true)
        |> Enum.map(fn take_str -> parse_take(take_str) end)
      end

    {game_id, takes}
  end

  def parse_take(take_str) do
    # 8 green, 6 blue, 20 red -> %{green => 8, blue => 6, red => 20}
    String.split(take_str, ",", trime: true)
    |> Enum.map(fn color_take_str ->
      [count_str, color] = String.split(color_take_str, " ", trim: true)
      %{color => String.to_integer(count_str)}
    end)
    |> Enum.reduce(%{}, fn take_map, acc ->
      Map.merge(acc, take_map)
    end)
  end

  def max_takes(takes) do
    Enum.reduce(takes, %{}, fn take, acc ->
      Enum.reduce(take, acc, fn {k, v}, acc ->
        Map.update(acc, k, v, fn prev_v ->
          if v > prev_v do
            v
          else
            prev_v
          end
        end)
      end)
    end)
  end
end
