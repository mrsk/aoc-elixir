defmodule Aoc2023.D05 do
  def solve_a() do
    File.read!("inputs/d05a.txt")
    |> parse_a()
    |> solve_parsed_a()
    |> IO.puts()
  end

  def solve_b() do
    File.read!("inputs/d05b.txt")
    |> parse_b()
    |> solve_parsed_b()
    |> IO.puts()
  end

  def solve_b2() do
    File.read!("inputs/d05b.txt")
    |> parse_b()
    |> solve_parsed_b2()
    |> IO.puts()
  end

  def seed_map_get(map, key) do
    Enum.reduce_while(map, key, fn
      {target, source, range}, acc ->
        if key >= source and key < source + range do
          {:halt, target - source + key}
        else
          {:cont, acc}
        end
    end)
  end

  def seed_reverse_map_get(map, key) do
    Enum.reduce_while(map, key, fn
      {target, source, range}, acc ->
        if key >= target and key < target + range do
          {:halt, source - target + key}
        else
          {:cont, acc}
        end
    end)
  end

  # xxx
  #     yyy
  def range_overwrite(x, x_range, y, _y_range, _y_overwrite, acc) when x + x_range <= y do
    [[x, x_range] | acc]
  end

  #     xxx
  # yyy
  def range_overwrite(x, x_range, y, y_range, _y_overwrite, acc) when x >= y + y_range do
    [[x, x_range] | acc]
  end

  #  x
  # yyy
  def range_overwrite(x, x_range, y, y_range, y_overwrite, acc)
      when x >= y and x + x_range <= y + y_range do
    [[y_overwrite + x - y, x_range] | acc]
  end

  # xxx
  #  yyy
  def range_overwrite(x, x_range, y, y_range, y_overwrite, acc)
      when x < y and x + x_range >= y and x + x_range <= y + y_range do
    [[x, y - x], [y_overwrite, x + x_range - y] | acc]
  end

  #  xxx
  # yyy
  def range_overwrite(x, x_range, y, y_range, y_overwrite, acc)
      when x >= y and x <= y + y_range and x + x_range > y + y_range do
    [[y_overwrite + x - y, y + y_range - x], [y + y_range, x + x_range - y - y_range] | acc]
  end

  # xxx
  #  y
  def range_overwrite(x, x_range, y, y_range, y_overwrite, acc)
      when x < y and x + x_range > y + y_range do
    [[x, y - x], [y_overwrite, y_range], [y + y_range, x + x_range - y - y_range] | acc]
  end

  def seed_map_merge(seeds, map) do
    Enum.reduce(map, seeds, fn {map_target, map_source, map_range}, seeds ->
      Enum.reduce(seeds, [], fn [seed, seed_range], new_seeds ->
        new_seeds =
          range_overwrite(seed, seed_range, map_source, map_range, map_target, new_seeds)

        new_seeds
      end)
    end)
  end

  def solve_parsed_a({seeds, maps}) do
    seeds_map = Enum.reduce(seeds, %{}, fn seed, seeds_map -> Map.put(seeds_map, seed, seed) end)

    Enum.reduce(maps, seeds_map, fn map, seeds_map ->
      Enum.reduce(seeds_map, seeds_map, fn {seed, target}, seeds_map ->
        Map.put(seeds_map, seed, seed_map_get(map, target))
      end)
    end)
    |> Map.values()
    |> Enum.min()
  end

  def solve_parsed_b({seeds, maps}) do
    Enum.reduce(maps, seeds, fn map, seeds ->
      seed_map_merge(seeds, map)
    end)
    |> Enum.map(fn [seed, _seed_range] -> seed end)
    |> Enum.min()
  end

  def solve_parsed_b2({seeds, maps}) do
    maps = Enum.reverse(maps)
    reverse_search({seeds, maps}, 0, false)
  end

  def reverse_search(_, location, true) do
    location - 1
  end

  def reverse_search({seeds, maps}, location, _found) do
    if rem(location, 1_000_000) === 0 do
      IO.puts("reverse search " <> Integer.to_string(location))
    end

    located_seed =
      Enum.reduce(maps, location, fn map, rev_location ->
        seed_reverse_map_get(map, rev_location)
      end)

    found =
      Enum.reduce_while(seeds, false, fn [seed, seed_range], _ ->
        if(located_seed >= seed and located_seed < seed + seed_range) do
          {:halt, true}
        else
          {:cont, false}
        end
      end)

    reverse_search({seeds, maps}, location + 1, found)
  end

  def parse_seeds_a(seeds_str) do
    # seeds: 79 14 55 13
    String.split(seeds_str, ":")
    |> Enum.at(1)
    |> String.split()
    |> Enum.map(&String.to_integer/1)
  end

  def parse_seeds_b(seeds_str) do
    # seeds: 79 14 55 13
    String.split(seeds_str, ":")
    |> Enum.at(1)
    |> String.split()
    |> Enum.map(&String.to_integer/1)
    |> Enum.chunk_every(2)
  end

  def parse_map_range(map_range_str, acc) do
    # 50 98 2
    [target, source, range] =
      String.split(map_range_str)
      |> Enum.map(&String.to_integer/1)

    [{target, source, range} | acc]
  end

  def parse_map(map_str) do
    # seed-to-soil map:
    # 50 98 2
    # 52 50 48
    String.split(map_str, ":\n")
    |> Enum.at(1)
    |> String.split("\n")
    |> Enum.reduce([], &parse_map_range/2)
  end

  def parse_a(contents) do
    [seeds_str | maps_str] =
      contents
      |> String.split("\n\n")

    seeds = parse_seeds_a(seeds_str)
    maps = Enum.map(maps_str, &parse_map/1)

    {seeds, maps}
  end

  def parse_b(contents) do
    [seeds_str | maps_str] =
      contents
      |> String.split("\n\n")

    seeds = parse_seeds_b(seeds_str)
    maps = Enum.map(maps_str, &parse_map/1)

    {seeds, maps}
  end
end
