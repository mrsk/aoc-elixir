defmodule Aoc2023.D03 do
  def solve_a() do
    solve_path("inputs/d03a.txt")
    |> IO.puts()
  end

  def solve_b() do
    solve_gears_path("inputs/d03b.txt")
    |> IO.puts()
  end

  def solve_path(path) do
    parse_path(path)
    |> solve_schematics()
  end

  def solve_gears_path(path) do
    parse_path(path)
    |> solve_gears()
  end

  def solve_gears(schematics) do
    schematics
    |> Enum.reduce(0, fn
      {{row, col}, "*"}, acc ->
        ratios = find_gear_ratios(schematics, row, col)

        if length(ratios) != 2 do
          acc
        else
          [r1, r2] = ratios
          acc + r1 * r2
        end

      _, acc ->
        acc
    end)
  end

  def find_gear_ratio(schematics, row, col) do
    ch = Map.get(schematics, {row, col})

    if is_numeric(ch) do
      ratio = find_gear_ratio(schematics, row, col - 1)

      if ratio != nil do
        ratio
      else
        ratio =
          scan_number_rec(schematics, row, col + 1, ch)
          |> elem(0)

        {row, col, ratio}
      end
    else
      nil
    end
  end

  def find_gear_ratios(schematics, row, col) do
    offsets = [{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}]

    Enum.reduce(offsets, MapSet.new(), fn
      {row_offset, col_offset}, acc ->
        gear_ratio = find_gear_ratio(schematics, row + row_offset, col + col_offset)

        if gear_ratio == nil do
          acc
        else
          MapSet.put(acc, gear_ratio)
        end
    end)
    |> Enum.map(fn {_row, _col, val} -> val end)
    |> Enum.to_list()
  end

  def solve_schematics(schematics) do
    Map.keys(schematics)
    |> Enum.sort()
    |> Enum.reduce(0, fn {row, col}, acc ->
      acc + scan_number(schematics, row, col)
    end)
  end

  def is_numeric(nil) do
    false
  end

  def is_numeric(ch) do
    Integer.parse(ch) != :error
  end

  def scan_number(schematics, row, col) do
    ch = Map.get(schematics, {row, col})

    if not is_numeric(ch) do
      0
    else
      ch_prev = Map.get(schematics, {row, col - 1})

      if is_numeric(ch_prev) do
        0
      else
        {number, length} = scan_number_rec(schematics, row, col + 1, ch)

        if(is_part_number(schematics, row, col, length)) do
          number
        else
          0
        end
      end
    end
  end

  def scan_number_rec(schematics, row, col, number_str) do
    ch = Map.get(schematics, {row, col})

    if not is_numeric(ch) do
      {String.to_integer(number_str), String.length(number_str)}
    else
      scan_number_rec(schematics, row, col + 1, number_str <> ch)
    end
  end

  def is_part_number(schematics, row, col, length) do
    row = row - 1
    col = col - 1
    is_part_number = false

    is_part_number =
      Enum.reduce_while(0..(length + 1), is_part_number, fn
        _, true ->
          {:halt, true}

        col_offset, false ->
          ch = Map.get(schematics, {row, col + col_offset})

          if(ch != nil and not is_numeric(ch)) do
            {:halt, true}
          else
            {:cont, false}
          end
      end)

    is_part_number =
      Enum.reduce_while(0..(length + 1), is_part_number, fn
        _, true ->
          {:halt, true}

        col_offset, false ->
          ch = Map.get(schematics, {row + 2, col + col_offset})

          if(ch != nil and not is_numeric(ch)) do
            {:halt, true}
          else
            {:cont, false}
          end
      end)

    is_part_number =
      Enum.reduce_while([0, length + 1], is_part_number, fn
        _, true ->
          {:halt, true}

        col_offset, false ->
          ch = Map.get(schematics, {row + 1, col + col_offset})

          if(ch != nil and not is_numeric(ch)) do
            {:halt, true}
          else
            {:cont, false}
          end
      end)

    is_part_number
  end

  def parse_path(path) do
    File.stream!(path)
    |> parse_lines()
  end

  def parse_lines(lines) do
    lines
    |> Stream.with_index()
    |> Enum.reduce(%{}, fn
      {line, line_i}, acc ->
        line = String.trim(line)

        String.graphemes(line)
        |> Stream.with_index()
        |> Enum.reduce(acc, fn
          {".", _ch_i}, acc ->
            acc

          {ch, ch_i}, acc ->
            Map.put(acc, {line_i, ch_i}, ch)
        end)
    end)
  end
end
