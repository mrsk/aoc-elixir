defmodule Aoc2023.D06 do
  def solve_a() do
    File.read!("inputs/d06a.txt")
    |> parse()
    |> solve_parsed_a()
    |> IO.puts()
  end

  def solve_b() do
    File.read!("inputs/d06b.txt")
    |> parse()
    |> solve_parsed_a()
    |> IO.puts()
  end

  def next_int(a, b, c, x) do
    if x * x * a + x * b + c <= 0 do
      next_int(a, b, c, x + 1)
    else
      x
    end
  end

  def prev_int(a, b, c, x) do
    if x * x * a + x * b + c <= 0 do
      prev_int(a, b, c, x - 1)
    else
      x
    end
  end

  def calc_gap(a, b, c) do
    d = b * b - 4 * a * c
    d_sqrt = :math.sqrt(d)
    r1 = -(b - d_sqrt) / (2 * a)
    r2 = -(b + d_sqrt) / (2 * a)
    b1 = trunc(Float.ceil(r1))
    b1 = next_int(a, b, c, b1 - 1)
    b2 = trunc(Float.floor(r2))
    b2 = prev_int(a, b, c, b2 + 1)
    b2 - b1 + 1
  end

  def solve_race_a({time, distance}, acc) do
    acc * calc_gap(-1, time, -distance)
  end

  def solve_parsed_a({time_list, distance_list}) do
    Enum.zip(time_list, distance_list)
    |> Enum.reduce(1, &solve_race_a/2)
  end

  def parse(contents) do
    String.split(contents, "\n")
    |> Enum.map(&parse_line/1)
    |> List.to_tuple()
  end

  def parse_line(line) do
    String.split(line, ":")
    |> Enum.at(1)
    |> String.split()
    |> Enum.map(&String.to_integer/1)
  end
end
