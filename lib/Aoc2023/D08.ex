defmodule Aoc2023.D08 do
  def solve_a() do
    File.read!("inputs/d08a.txt")
    |> parse()
    |> solve_parsed_a()
    |> IO.puts()
  end

  def solve_b() do
    File.read!("inputs/d08b.txt")
    |> parse()
    |> solve_parsed_b()
    |> IO.puts()
  end

  def solve_parsed_a({directions, desert}) do
    Stream.iterate(0, &(&1 + 1))
    |> Enum.reduce_while({"AAA", 0}, fn _, {pos, distance} ->
      if pos == "ZZZ" do
        {:halt, {pos, distance}}
      else
        direction_i = rem(distance, tuple_size(directions))
        {:cont, {Map.fetch!(desert, pos) |> elem(elem(directions, direction_i)), distance + 1}}
      end
    end)
    |> elem(1)
  end

  def find_positions(desert, third_letter) do
    Enum.reduce(desert, [], fn {k, _v}, acc ->
      if String.at(k, 2) == third_letter do
        [k | acc]
      else
        acc
      end
    end)
  end

  def find_shortcut(directions, desert, pos, shortcuts) do
    {pos_distance, pos_name} = pos
    directions_len = tuple_size(directions)
    pos_direction_i = rem(pos_distance, directions_len)

    shortcut = Map.get(shortcuts, {pos_direction_i, pos_name})

    if shortcut != nil do
      {elem(shortcut, 0), elem(shortcut, 1), shortcuts}
    else
      new_pos_name =
        Map.fetch!(desert, pos_name) |> elem(elem(directions, pos_direction_i))

      Stream.cycle([nil])
      |> Enum.reduce_while(
        {1, new_pos_name, shortcuts},
        fn _, {distance, new_pos_name, shortcuts} ->
          if String.at(new_pos_name, 2) == "Z" do
            shortcuts =
              Map.put(shortcuts, {pos_direction_i, pos_name}, {distance, new_pos_name})

            {:halt, {distance, new_pos_name, shortcuts}}
          else
            direction_i = rem(distance, directions_len)

            new_pos_name =
              Map.fetch!(desert, new_pos_name) |> elem(elem(directions, direction_i))

            {:cont, {distance + 1, new_pos_name, shortcuts}}
          end
        end
      )
    end
  end

  def go_next(directions, desert, pos, shortcuts) do
    {distance, _pos_name} = pos

    {shortcut_distance, shortcut_name, shortcuts} =
      find_shortcut(directions, desert, pos, shortcuts)

    {{distance + shortcut_distance, shortcut_name}, shortcuts}
  end

  def find_z(directions, desert, poss) do
    Enum.reduce(poss, [], fn pos, poss ->
      Stream.iterate(0, &(&1 + 1))
      |> Enum.reduce_while(pos, fn _direction_i, {distance, pos_name} ->
        if String.at(pos_name, 2) == "Z" do
          # IO.inspect(List.to_tuple(["find_z", {distance, pos_name} | poss]))
          {:halt, [{distance, pos_name} | poss]}
        else
          direction_i = rem(distance, tuple_size(directions))

          {:cont,
           {distance + 1, Map.fetch!(desert, pos_name) |> elem(elem(directions, direction_i))}}
        end
      end)
    end)
  end

  def solve_parsed_b({directions, desert}) do
    poss =
      find_positions(desert, "A")
      |> Enum.map(&{0, &1})

    poss = find_z(directions, desert, poss)
    shortcuts = %{}

    Stream.cycle([nil])
    |> Enum.reduce_while({poss, shortcuts}, fn _, {poss, shortcuts} ->
      pos_max = Enum.max(poss)
      pos_min = Enum.min(poss)

      pos_max_distance = elem(pos_max, 0)
      pos_min_distance = elem(pos_min, 0)

      if pos_max_distance == pos_min_distance do
        {:halt, {poss, shortcuts}}
      else
        acc =
          Enum.reduce(poss, {[], shortcuts}, fn pos, {poss_acc, shortcuts} ->
            if elem(pos, 0) == pos_max_distance do
              {[pos | poss_acc], shortcuts}
            else
              {pos, shortcuts} = go_next(directions, desert, pos, shortcuts)
              {[pos | poss_acc], shortcuts}
            end
          end)

        {:cont, acc}
      end
    end)
    |> elem(0)
    |> Enum.at(0)
    |> elem(0)
  end

  def parse_desert_line(line, acc) do
    # CTK = (JLT, HRF)
    <<k::binary-size(3)>> <>
      " = (" <> <<l::binary-size(3)>> <> ", " <> <<r::binary-size(3)>> <> ")" = line

    Map.put(acc, k, {l, r})
  end

  def parse_desert(desert_str) do
    String.split(desert_str, "\n")
    |> Enum.reduce(%{}, &parse_desert_line/2)
  end

  def parse(contents) do
    [directions_str, desert_str] = String.split(contents, "\n\n")

    directions =
      directions_str
      |> String.replace("L", "0")
      |> String.replace("R", "1")
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> List.to_tuple()

    desert = parse_desert(desert_str)

    {directions, desert}
  end
end
