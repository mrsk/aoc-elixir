defmodule Aoc2023.D01 do
  def first_number_char(s) do
    s
    |> String.graphemes()
    |> Enum.find(nil, fn c -> Integer.parse(c) != :error end)
  end

  def parse_calibration_value_a(line) do
    line_reversed = String.reverse(line)

    (first_number_char(line) <> first_number_char(line_reversed))
    |> Integer.parse()
    |> case do
      :error -> 0
      x -> elem(x, 0)
    end
  end

  def parse_calibration_value_b(line) do
    line_reversed = String.reverse(line)

    line =
      String.replace(
        line,
        ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"],
        fn
          "one" -> "1"
          "two" -> "2"
          "three" -> "3"
          "four" -> "4"
          "five" -> "5"
          "six" -> "6"
          "seven" -> "7"
          "eight" -> "8"
          "nine" -> "9"
          x -> raise("replace error: " <> x)
        end,
        global: false
      )

    line_reversed =
      String.replace(
        line_reversed,
        ["eno", "owt", "eerht", "ruof", "evif", "xis", "neves", "thgie", "enin"],
        fn
          "eno" -> "1"
          "owt" -> "2"
          "eerht" -> "3"
          "ruof" -> "4"
          "evif" -> "5"
          "xis" -> "6"
          "neves" -> "7"
          "thgie" -> "8"
          "enin" -> "9"
          x -> raise("replace error: " <> x)
        end,
        global: false
      )

    (first_number_char(line) <> first_number_char(line_reversed))
    |> Integer.parse()
    |> case do
      :error -> 0
      x -> elem(x, 0)
    end
  end

  def solve_a() do
    solve_path("inputs/d01a.txt", &parse_calibration_value_a/1)
    |> IO.puts()
  end

  def solve_b() do
    solve_path("inputs/d01b.txt", &parse_calibration_value_b/1)
    |> IO.puts()
  end

  def solve_path(path, calibration_parser) do
    File.stream!(path)
    |> solve_stream(calibration_parser)
  end

  def solve_stream(lines, calibration_parser) do
    lines
    |> Stream.map(fn line -> calibration_parser.(line) end)
    |> Enum.reduce(0, fn x, acc -> x + acc end)
  end
end
