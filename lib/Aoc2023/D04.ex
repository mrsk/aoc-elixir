defmodule Aoc2023.D04 do
  def solve_a() do
    solve_path_a("inputs/d04a.txt")
    |> IO.puts()
  end

  def solve_b() do
    solve_path_b("inputs/d04b.txt")
    |> IO.puts()
  end

  def solve_path_a(path) do
    File.stream!(path)
    |> Stream.map(&solve_line_a/1)
    |> Enum.sum()
  end

  def solve_path_b(path) do
    File.stream!(path)
    |> Enum.reduce(%{}, &solve_line_b/2)
    |> Map.get(:total, 0)
  end

  def parse_nums(nums_str) do
    String.split(nums_str)
    |> Enum.map(&String.to_integer/1)
    |> MapSet.new()
  end

  def parse_line(line) do
    [card, wnums_nums] = String.split(line, ":")
    card = String.split(card) |> Enum.at(1) |> String.to_integer()
    [wnums, nums] = String.split(wnums_nums, "|")
    [card, parse_nums(wnums), parse_nums(nums)]
  end

  def matching_length(wnums, nums) do
    MapSet.intersection(wnums, nums)
    |> Enum.to_list()
    |> length()
  end

  def solve_line_a(line) do
    [_card, wnums, nums] = parse_line(line)
    matches = matching_length(wnums, nums)

    case matches do
      0 -> 0
      m -> Integer.pow(2, m - 1)
    end
  end

  def solve_line_b(line, counts_acc) do
    [card, wnums, nums] = parse_line(line)
    matches = matching_length(wnums, nums)
    card_amount = Map.get(counts_acc, card, 1)
    counts_acc = Map.update(counts_acc, :total, card_amount, &(&1 + card_amount))

    if matches == 0 do
      counts_acc
    else
      Enum.reduce((card + 1)..(card + matches), counts_acc, fn
        win_card, counts_acc ->
          Map.update(counts_acc, win_card, 1 + card_amount, &(&1 + card_amount))
      end)
    end
  end
end
