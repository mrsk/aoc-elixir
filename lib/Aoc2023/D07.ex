defmodule Aoc2023.D07 do
  def solve_a() do
    File.read!("inputs/d07a.txt")
    |> parse()
    |> solve_parsed_a()
    |> IO.puts()
  end

  def solve_b() do
    File.read!("inputs/d07b.txt")
    |> parse()
    |> solve_parsed_b()
    |> IO.puts()
  end

  def solve_parsed_a(hands) do
    hands
    |> Enum.map(fn {cards, bid} -> {calc_score(cards), bid} end)
    |> Enum.sort()
    |> Enum.with_index(1)
    |> Enum.reduce(0, fn {{_, bid}, rank}, acc -> acc + rank * bid end)
  end

  def solve_parsed_b(hands) do
    hands
    |> Enum.map(fn {cards, bid} -> {calc_score_j(cards), bid} end)
    |> Enum.sort()
    |> Enum.with_index(1)
    |> Enum.reduce(0, fn {{_, bid}, rank}, acc -> acc + rank * bid end)
  end

  def calc_score(cards) do
    cards =
      cards
      |> String.replace("A", "E")
      |> String.replace("K", "D")
      |> String.replace("Q", "C")
      |> String.replace("J", "B")
      |> String.replace("T", "A")

    counts =
      String.graphemes(cards)
      |> Enum.reduce(%{}, fn c, acc ->
        Map.update(acc, c, 1, &(&1 + 1))
      end)

    cond do
      five_of_a_kind?(counts) -> {6, cards}
      four_of_a_kind?(counts) -> {5, cards}
      full_house?(counts) -> {4, cards}
      three_of_a_kind?(counts) -> {3, cards}
      two_pair?(counts) -> {2, cards}
      one_pair?(counts) -> {1, cards}
      true -> {0, cards}
    end
  end

  def calc_score_j(cards) do
    cards =
      cards
      |> String.replace("A", "E")
      |> String.replace("K", "D")
      |> String.replace("Q", "C")
      |> String.replace("J", "!")
      |> String.replace("T", "A")

    counts =
      String.graphemes(cards)
      |> Enum.reduce(%{}, fn c, acc ->
        Map.update(acc, c, 1, &(&1 + 1))
      end)

    cond do
      five_of_a_kind_j?(counts) -> {6, cards}
      four_of_a_kind_j?(counts) -> {5, cards}
      full_house_j?(counts) -> {4, cards}
      three_of_a_kind_j?(counts) -> {3, cards}
      two_pair_j?(counts) -> {2, cards}
      one_pair_j?(counts) -> {1, cards}
      true -> {0, cards}
    end
  end

  def split_j_counts(counts) do
    j_count = Map.get(counts, "!", 0)
    counts = Map.delete(counts, "!")
    {j_count, counts}
  end

  def max_count(counts) do
    Map.values(counts)
    |> Enum.max()
  end

  def five_of_a_kind?(counts), do: map_size(counts) == 1

  def four_of_a_kind?(counts) do
    Enum.reduce_while(counts, false, fn {_k, v}, _acc ->
      if v == 4 do
        {:halt, true}
      else
        {:cont, false}
      end
    end)
  end

  def full_house?(counts), do: map_size(counts) == 2

  def three_of_a_kind?(counts) do
    Enum.reduce_while(counts, false, fn {_k, v}, _acc ->
      if v == 3 do
        {:halt, true}
      else
        {:cont, false}
      end
    end)
  end

  def two_pair?(counts) do
    2 ==
      counts
      |> Enum.filter(fn {_k, v} -> v == 2 end)
      |> length
  end

  def one_pair?(counts) do
    Enum.reduce_while(counts, false, fn {_k, v}, _acc ->
      if v == 2 do
        {:halt, true}
      else
        {:cont, false}
      end
    end)
  end

  def five_of_a_kind_j?(counts) do
    if five_of_a_kind?(counts) do
      true
    else
      {j_count, counts} = split_j_counts(counts)
      max_count = max_count(counts)
      max_count + j_count >= 5
    end
  end

  def four_of_a_kind_j?(counts) do
    if four_of_a_kind?(counts) do
      true
    else
      {j_count, counts} = split_j_counts(counts)
      max_count = max_count(counts)
      max_count + j_count >= 4
    end
  end

  def full_house_j?(counts) do
    if full_house?(counts) do
      true
    else
      {j_count, counts} = split_j_counts(counts)

      two_pair?(counts) and j_count == 1
      # else upgrades to four_of_a_kind_j
    end
  end

  def three_of_a_kind_j?(counts) do
    if three_of_a_kind?(counts) do
      true
    else
      {j_count, counts} = split_j_counts(counts)
      (one_pair?(counts) and j_count == 1) or j_count > 1
    end
  end

  def two_pair_j?(counts) do
    two_pair?(counts)
  end

  def one_pair_j?(counts) do
    {j_count, counts} = split_j_counts(counts)
    max_count = max_count(counts)
    j_count > 0 or max_count == 2
  end

  def parse(contents) do
    contents
    |> String.split("\n")
    |> Enum.map(&parse_line/1)
  end

  def parse_line(line) do
    [cards, bid] = String.split(line)
    {cards, String.to_integer(bid)}
  end
end
