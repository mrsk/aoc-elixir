defmodule Aoc2023.D10 do
  def solve_a() do
    File.read!("inputs/d10a.txt")
    |> parse()
    |> solve_parsed_a()
    |> IO.puts()
  end

  def solve_parsed_a(pipes) do
    pos_s = find_pipe(pipes, fn {_x, _y, p} -> p == "S" end)
    {x_s, y_s, _} = pos_s

    [pos_next | _] = get_s_connections(pipes, x_s, y_s)

    loop_size =
      travel(pipes, pos_s, pos_next, 1, fn _pos, acc ->
        {:cont, acc + 1}
      end)

    div(loop_size + 1, 2)
  end

  def get_s_connections(pipes, x, y) do
    offsets = [{0, 1}, {0, -1}, {1, 0}, {-1, 0}]

    Enum.reduce(offsets, [], fn {dx, dy}, acc ->
      x_next = x + dx
      y_next = y + dy
      p_next = get_pipe(pipes, x_next, y_next)
      pos_next = {x_next, y_next, p_next}
      next_next = get_next_xy({x, y, "S"}, pos_next)

      cond do
        next_next == :error -> acc
        true -> [pos_next | acc]
      end
    end)
  end

  def get_next_xy({_, _, _}, {x, y, "S"}), do: {x, y}

  # N
  def get_next_xy({x_prev, y_prev, _}, {x, y, p}) when x_prev == x and y_prev == y + 1 do
    case p do
      "|" -> {x, y - 1}
      "7" -> {x - 1, y}
      "F" -> {x + 1, y}
      _ -> :error
    end
  end

  # S
  def get_next_xy({x_prev, y_prev, _}, {x, y, p}) when x_prev == x and y_prev == y - 1 do
    case p do
      "|" -> {x, y + 1}
      "L" -> {x + 1, y}
      "J" -> {x - 1, y}
      _ -> :error
    end
  end

  # W
  def get_next_xy({x_prev, y_prev, _}, {x, y, p}) when x_prev == x + 1 and y_prev == y do
    case p do
      "-" -> {x - 1, y}
      "L" -> {x, y - 1}
      "F" -> {x, y + 1}
      _ -> :error
    end
  end

  # E
  def get_next_xy({x_prev, y_prev, _}, {x, y, p}) when x_prev == x - 1 and y_prev == y do
    case p do
      "-" -> {x + 1, y}
      "J" -> {x, y - 1}
      "7" -> {x, y + 1}
      _ -> :error
    end
  end

  # ..
  def get_next_xy(_, _), do: :error

  def tuple_at(_t, i) when i < 0, do: nil
  def tuple_at(t, i) when i >= tuple_size(t), do: nil
  def tuple_at(t, i), do: elem(t, i)

  def get_pipe(pipes, x, y) do
    pipes
    |> tuple_at(y)
    |> tuple_at(x)
  end

  def find_pipe(pipes, cond_fn) do
    Enum.reduce_while(0..(tuple_size(pipes) - 1), nil, fn y, _ ->
      pipes_y = pipes |> elem(y)

      case Enum.reduce_while(0..(tuple_size(pipes_y) - 1), nil, fn x, _ ->
             pipe = pipes_y |> elem(x)

             if cond_fn.({x, y, pipe}) do
               {:halt, {x, y, pipe}}
             else
               {:cont, nil}
             end
           end) do
        nil -> {:cont, nil}
        acc -> {:halt, acc}
      end
    end)
  end

  def travel(pipes, pos_prev, pos, acc, f) do
    {x, y} = get_next_xy(pos_prev, pos)
    p = get_pipe(pipes, x, y)
    pos_next = {x, y, p}

    if p == "S" do
      {_, acc} = f.(pos, acc)
      acc
    else
      case f.(pos, acc) do
        {:cont, acc} -> travel(pipes, pos, pos_next, acc, f)
        {:halt, acc} -> acc
      end
    end
  end

  def parse(contents) do
    String.split(contents)
    |> Enum.map(&String.graphemes/1)
    |> Enum.map(&List.to_tuple/1)
    |> List.to_tuple()
  end
end
