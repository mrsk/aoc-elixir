# Aoc2023

```bash
mix eval Aoc2023.D01.solve_a
mix eval Aoc2023.D01.solve_b
```

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `aoc2023` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:aoc2023, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at <https://hexdocs.pm/aoc2023>.

