defmodule Aoc2023.D06.Test do
  use ExUnit.Case

  test "inputs a" do
    assert File.read!("inputs/d06a_test_0.txt")
           |> Aoc2023.D06.parse()
           |> Aoc2023.D06.solve_parsed_a() == 288
  end
end
