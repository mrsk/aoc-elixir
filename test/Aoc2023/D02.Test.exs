defmodule Aoc2023.D02.Test do
  use ExUnit.Case

  test "parse_take" do
    assert Aoc2023.D02.parse_take("1 red") == %{"red" => 1}

    assert Aoc2023.D02.parse_take("8 green, 6 blue, 20 red") == %{
             "green" => 8,
             "blue" => 6,
             "red" => 20
           }
  end

  test "d02a_test.txt" do
    assert Aoc2023.D02.solve_path(
             "inputs/d02a_test.txt",
             12,
             13,
             14,
             &Aoc2023.D02.solve_line_a/4
           ) == 8
  end

  test "solve_line_a" do
    assert Aoc2023.D02.solve_line_a(
             "Game 68: 5 green, 3 blue, 2 red; 4 green, 8 blue, 11 red; 6 red, 6 blue, 4 green; 8 red, 5 blue, 7 green; 6 blue, 6 green, 11 red; 2 blue, 3 green, 3 red",
             12,
             13,
             14
           ) == 68

    assert Aoc2023.D02.solve_line_a(
             "Game 90: 8 red, 7 blue; 4 green, 3 red, 1 blue; 5 blue, 2 green",
             12,
             13,
             14
           ) == 90

    assert Aoc2023.D02.solve_line_a(
             "Game 1: ",
             12,
             13,
             14
           ) == 1

    assert Aoc2023.D02.solve_line_a(
             "Game 1: 12 red; 13 green; 14 blue",
             12,
             13,
             14
           ) == 1

    assert Aoc2023.D02.solve_line_a(
             "Game 1: 12 red, 13 green, 14 blue",
             12,
             13,
             14
           ) == 1
  end

  test "d02b_test.txt" do
    assert Aoc2023.D02.solve_path(
             "inputs/d02b_test.txt",
             12,
             13,
             14,
             &Aoc2023.D02.solve_line_b/4
           ) == 2286
  end
end
