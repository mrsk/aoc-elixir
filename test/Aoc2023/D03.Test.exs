defmodule Aoc2023.D03.Test do
  use ExUnit.Case

  test "inputs a" do
    assert Aoc2023.D03.solve_path("./inputs/d03a_test_0.txt") == 4361
    assert Aoc2023.D03.solve_path("./inputs/d03a_test_1.txt") == 0
    assert Aoc2023.D03.solve_path("./inputs/d03a_test_2.txt") == 99
  end

  test "inputs b" do
    assert Aoc2023.D03.solve_gears_path("./inputs/d03b_test_0.txt") == 467_835
  end

  test "parse_lines" do
    assert Aoc2023.D03.parse_lines(["..."]) == %{}
    assert Aoc2023.D03.parse_lines([".x."]) == %{{0, 1} => "x"}
    assert Aoc2023.D03.parse_lines(["...", "..y", "..."]) == %{{1, 2} => "y"}

    assert Aoc2023.D03.parse_lines(["..x", ".y.", "z.."]) == %{
             {0, 2} => "x",
             {1, 1} => "y",
             {2, 0} => "z"
           }
  end

  test "is_part_number" do
    schematics_1 = Aoc2023.D03.parse_path("./inputs/d03a_test_1.txt")
    assert Aoc2023.D03.is_part_number(schematics_1, 0, 0, 2) == false
    assert Aoc2023.D03.is_part_number(schematics_1, 0, 3, 2) == false
    assert Aoc2023.D03.is_part_number(schematics_1, 0, 6, 2) == false
    assert Aoc2023.D03.is_part_number(schematics_1, 2, 0, 2) == false
    assert Aoc2023.D03.is_part_number(schematics_1, 2, 3, 2) == false
    assert Aoc2023.D03.is_part_number(schematics_1, 2, 6, 2) == false
    assert Aoc2023.D03.is_part_number(schematics_1, 4, 0, 2) == false
    assert Aoc2023.D03.is_part_number(schematics_1, 4, 3, 2) == false
    assert Aoc2023.D03.is_part_number(schematics_1, 4, 6, 2) == false
  end

  test "find_gear_ratio" do
    schematics_0 = Aoc2023.D03.parse_path("./inputs/d03b_test_0.txt")
    assert Aoc2023.D03.find_gear_ratio(schematics_0, 0, 2) == {0, 0, 467}
  end
end
