defmodule Aoc2023.D07.Test do
  use ExUnit.Case

  test "inputs a" do
    assert File.read!("inputs/d07a_test_0.txt")
           |> Aoc2023.D07.parse()
           |> Aoc2023.D07.solve_parsed_a() == 6440
  end

  test "inputs b" do
    assert File.read!("inputs/d07b_test_0.txt")
           |> Aoc2023.D07.parse()
           |> Aoc2023.D07.solve_parsed_b() == 5905
  end

  test "calc_score_j" do
    # five of a kind
    assert Aoc2023.D07.calc_score_j("FFFFF") |> elem(0) == 6
    assert Aoc2023.D07.calc_score_j("FFFFJ") |> elem(0) == 6
    assert Aoc2023.D07.calc_score_j("FFFJJ") |> elem(0) == 6
    assert Aoc2023.D07.calc_score_j("FFJJJ") |> elem(0) == 6
    assert Aoc2023.D07.calc_score_j("FJJJJ") |> elem(0) == 6
    assert Aoc2023.D07.calc_score_j("JJJJJ") |> elem(0) == 6

    # four of a kind
    assert Aoc2023.D07.calc_score_j("FFFFB") |> elem(0) == 5
    assert Aoc2023.D07.calc_score_j("FFFBJ") |> elem(0) == 5
    assert Aoc2023.D07.calc_score_j("FFBJJ") |> elem(0) == 5
    assert Aoc2023.D07.calc_score_j("FBJJJ") |> elem(0) == 5

    # full house
    assert Aoc2023.D07.calc_score_j("FFFBB") |> elem(0) == 4
    assert Aoc2023.D07.calc_score_j("FFJBB") |> elem(0) == 4

    # three of a kind
    assert Aoc2023.D07.calc_score_j("FFFBC") |> elem(0) == 3
    assert Aoc2023.D07.calc_score_j("FFBCJ") |> elem(0) == 3
    assert Aoc2023.D07.calc_score_j("FBCJJ") |> elem(0) == 3

    # two pair
    assert Aoc2023.D07.calc_score_j("FFBBC") |> elem(0) == 2

    # one pair
    assert Aoc2023.D07.calc_score_j("FFBCD") |> elem(0) == 1
    assert Aoc2023.D07.calc_score_j("FBCDJ") |> elem(0) == 1

    # nothing
    assert Aoc2023.D07.calc_score_j("FBCDE") |> elem(0) == 0
  end
end
