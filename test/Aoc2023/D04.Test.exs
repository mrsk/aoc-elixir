defmodule Aoc2023.D04.Test do
  use ExUnit.Case

  test "inputs a" do
    assert Aoc2023.D04.solve_path_a("./inputs/d04a_test_0.txt") == 13
  end

  test "inputs b" do
    assert Aoc2023.D04.solve_path_b("./inputs/d04b_test_0.txt") == 30
  end
end
