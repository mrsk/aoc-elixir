defmodule Aoc2023.D09.Test do
  use ExUnit.Case

  test "inputs a" do
    assert File.stream!("inputs/d09a_test_0.txt")
           |> Stream.map(&Aoc2023.D09.parse_line/1)
           |> Stream.map(&Enum.reverse/1)
           |> Stream.map(&Aoc2023.D09.solve_line_a/1)
           |> Enum.sum() ==
             114
  end

  test "inputs b" do
    assert File.stream!("inputs/d09b_test_0.txt")
           |> Stream.map(&Aoc2023.D09.parse_line/1)
           |> Stream.map(&Aoc2023.D09.solve_line_a/1)
           |> Enum.sum() ==
             2
  end
end
