defmodule Aoc2023.D01.Test do
  use ExUnit.Case

  test "d01a_test.txt" do
    assert Aoc2023.D01.solve_path(
             "inputs/d01a_test.txt",
             &Aoc2023.D01.parse_calibration_value_a/1
           ) == 142
  end

  test "d01b_test.txt" do
    assert Aoc2023.D01.solve_path(
             "inputs/d01b_test.txt",
             &Aoc2023.D01.parse_calibration_value_b/1
           ) == 281
  end
end
