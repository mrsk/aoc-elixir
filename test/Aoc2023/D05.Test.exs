defmodule Aoc2023.D05.Test do
  use ExUnit.Case

  test "inputs a" do
    assert File.read!("inputs/d05a_test_0.txt")
           |> Aoc2023.D05.parse_a()
           |> Aoc2023.D05.solve_parsed_a() == 35
  end

  test "inputs b" do
    assert File.read!("inputs/d05b_test_0.txt")
           |> Aoc2023.D05.parse_b()
           |> Aoc2023.D05.solve_parsed_b() == 46
  end

  test "parse_seeds_a" do
    assert Aoc2023.D05.parse_seeds_a("seeds: 79 14 55 13") == [79, 14, 55, 13]
  end

  test "parse_map_range" do
    assert Aoc2023.D05.parse_map_range("50 98 2", []) == [{50, 98, 2}]
  end

  test "parse_seeds_b" do
    assert Aoc2023.D05.parse_seeds_b("seeds: 79 14 55 13") == [[79, 14], [55, 13]]
  end

  test "seed_map_merge" do
    # |     x xxx
    # |yyy y
    assert Aoc2023.D05.seed_map_merge([[5, 1], [7, 3]], [{100, 0, 3}, {104, 4, 1}])
           |> Enum.sort() == [[5, 1], [7, 3]]

    # |     x xxx
    # | yyy y
    assert Aoc2023.D05.seed_map_merge([[5, 1], [7, 3]], [{101, 1, 3}, {105, 5, 1}])
           |> Enum.sort() == [[7, 3], [105, 1]]

    # |     x xxx
    # |  yyy y
    assert Aoc2023.D05.seed_map_merge([[5, 1], [7, 3]], [{102, 2, 3}, {106, 6, 1}])
           |> Enum.sort() == [[5, 1], [7, 3]]

    # |     x xxx
    # |   yyy y
    assert Aoc2023.D05.seed_map_merge([[5, 1], [7, 3]], [{103, 3, 3}, {107, 7, 1}])
           |> Enum.sort() == [[8, 2], [105, 1], [107, 1]]

    # |     x xxx
    # |    yyy y
    assert Aoc2023.D05.seed_map_merge([[5, 1], [7, 3]], [{104, 4, 3}, {108, 8, 1}])
           |> Enum.sort() == [[7, 1], [9, 1], [105, 1], [108, 1]]

    # |     x xxx
    # |     yyy y
    assert Aoc2023.D05.seed_map_merge([[5, 1], [7, 3]], [{105, 5, 3}, {109, 9, 1}])
           |> Enum.sort() == [[8, 1], [105, 1], [107, 1], [109, 1]]

    # |     x xxx
    # |      yyy y
    assert Aoc2023.D05.seed_map_merge([[5, 1], [7, 3]], [{106, 6, 3}, {110, 10, 1}])
           |> Enum.sort() == [[5, 1], [9, 1], [107, 2]]

    # |     x xxx
    # |       yyy y
    assert Aoc2023.D05.seed_map_merge([[5, 1], [7, 3]], [{107, 7, 3}, {111, 11, 1}])
           |> Enum.sort() == [[5, 1], [107, 3]]

    # |     x xxx
    # |        yyy y
    assert Aoc2023.D05.seed_map_merge([[5, 1], [7, 3]], [{108, 8, 3}, {112, 12, 1}])
           |> Enum.sort() == [[5, 1], [7, 1], [108, 2]]

    # |     x xxx
    # |         yyy y
    assert Aoc2023.D05.seed_map_merge([[5, 1], [7, 3]], [{109, 9, 3}, {113, 13, 1}])
           |> Enum.sort() == [[5, 1], [7, 2], [109, 1]]

    # |     x xxx
    # |          yyy y
    assert Aoc2023.D05.seed_map_merge([[5, 1], [7, 3]], [{110, 10, 3}, {114, 14, 1}])
           |> Enum.sort() == [[5, 1], [7, 3]]
  end

  test "range_overwrite" do
    # xxx
    #     yyy
    assert Aoc2023.D05.range_overwrite(0, 3, 5, 3, 15, []) == [[0, 3]]

    #     xxx
    # yyy
    assert Aoc2023.D05.range_overwrite(5, 3, 0, 3, 10, []) == [[5, 3]]

    #  x
    # yyy
    assert Aoc2023.D05.range_overwrite(1, 1, 0, 3, 10, []) == [[11, 1]]

    # xxx
    #  yyy
    assert Aoc2023.D05.range_overwrite(0, 3, 1, 3, 11, []) == [[0, 1], [11, 2]]

    #  xxx
    # yyy
    assert Aoc2023.D05.range_overwrite(1, 3, 0, 3, 10, []) == [[11, 2], [3, 1]]

    # xxx
    #  y
    assert Aoc2023.D05.range_overwrite(0, 3, 1, 1, 11, []) == [[0, 1], [11, 1], [2, 1]]

    ######

    # x
    #  y
    assert Aoc2023.D05.range_overwrite(0, 1, 1, 1, 11, []) == [[0, 1]]

    #  x
    # y
    assert Aoc2023.D05.range_overwrite(1, 1, 0, 1, 10, []) == [[1, 1]]

    # x
    # y
    assert Aoc2023.D05.range_overwrite(0, 1, 0, 1, 10, []) == [[10, 1]]

    # xx
    #  yy
    assert Aoc2023.D05.range_overwrite(0, 2, 1, 2, 11, []) == [[0, 1], [11, 1]]

    #  xx
    # yy
    assert Aoc2023.D05.range_overwrite(1, 2, 0, 2, 10, []) == [[11, 1], [2, 1]]

    # xx
    #  y
    assert Aoc2023.D05.range_overwrite(0, 2, 1, 1, 11, []) == [[0, 1], [11, 1]]

    #  xx
    #  y
    assert Aoc2023.D05.range_overwrite(1, 2, 1, 1, 11, []) == [[11, 1], [2, 1]]

    #  x
    # yy
    assert Aoc2023.D05.range_overwrite(1, 1, 0, 2, 10, []) == [[11, 1]]

    #  x
    #  yy
    assert Aoc2023.D05.range_overwrite(1, 1, 1, 2, 11, []) == [[11, 1]]

    #################

    # xxx
    #     yyy
    assert Aoc2023.D05.range_overwrite(1, 3, 6, 3, 16, []) == [[1, 3]]

    #     xxx
    # yyy
    assert Aoc2023.D05.range_overwrite(6, 3, 1, 3, 11, []) == [[6, 3]]

    #  x
    # yyy
    assert Aoc2023.D05.range_overwrite(2, 1, 1, 3, 11, []) == [[12, 1]]

    # xxx
    #  yyy
    assert Aoc2023.D05.range_overwrite(1, 3, 2, 3, 12, []) == [[1, 1], [12, 2]]

    #  xxx
    # yyy
    assert Aoc2023.D05.range_overwrite(2, 3, 1, 3, 11, []) == [[12, 2], [4, 1]]

    # xxx
    #  y
    assert Aoc2023.D05.range_overwrite(1, 3, 2, 1, 12, []) == [[1, 1], [12, 1], [3, 1]]

    ######

    # x
    #  y
    assert Aoc2023.D05.range_overwrite(1, 1, 2, 1, 12, []) == [[1, 1]]

    #  x
    # y
    assert Aoc2023.D05.range_overwrite(2, 1, 1, 1, 11, []) == [[2, 1]]

    # x
    # y
    assert Aoc2023.D05.range_overwrite(1, 1, 1, 1, 11, []) == [[11, 1]]

    # xx
    #  yy
    assert Aoc2023.D05.range_overwrite(1, 2, 2, 2, 12, []) == [[1, 1], [12, 1]]

    #  xx
    # yy
    assert Aoc2023.D05.range_overwrite(2, 2, 1, 2, 11, []) == [[12, 1], [3, 1]]

    # xx
    #  y
    assert Aoc2023.D05.range_overwrite(1, 2, 2, 1, 12, []) == [[1, 1], [12, 1]]

    #  xx
    #  y
    assert Aoc2023.D05.range_overwrite(2, 2, 2, 1, 12, []) == [[12, 1], [3, 1]]

    #  x
    # yy
    assert Aoc2023.D05.range_overwrite(2, 1, 1, 2, 11, []) == [[12, 1]]

    #  x
    #  yy
    assert Aoc2023.D05.range_overwrite(2, 1, 2, 2, 12, []) == [[12, 1]]

    #############

    assert Aoc2023.D05.range_overwrite(5, 1, 5, 1, 105, [[7, 3]]) == [[105, 1], [7, 3]]
  end
end
