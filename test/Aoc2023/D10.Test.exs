defmodule Aoc2023.D10.Test do
  use ExUnit.Case

  test "inputs a" do
    assert File.read!("inputs/d10a_test_0.txt")
           |> Aoc2023.D10.parse()
           |> Aoc2023.D10.solve_parsed_a() == 4

    assert File.read!("inputs/d10a_test_1.txt")
           |> Aoc2023.D10.parse()
           |> Aoc2023.D10.solve_parsed_a() == 8
  end
end
