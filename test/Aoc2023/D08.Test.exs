defmodule Aoc2023.D08.Test do
  use ExUnit.Case

  test "inputs a" do
    assert File.read!("inputs/d08a_test_0.txt")
           |> Aoc2023.D08.parse()
           |> Aoc2023.D08.solve_parsed_a() == 2

    assert File.read!("inputs/d08a_test_1.txt")
           |> Aoc2023.D08.parse()
           |> Aoc2023.D08.solve_parsed_a() == 6
  end

  test "inputs b" do
    assert File.read!("inputs/d08b_test_0.txt")
           |> Aoc2023.D08.parse()
           |> Aoc2023.D08.solve_parsed_b() == 6
  end

  test "parse_desert_line" do
    assert Aoc2023.D08.parse_desert_line("CTK = (JLT, HRF)", %{}) == %{"CTK" => {"JLT", "HRF"}}
  end
end
